@extends('layout.master')

@section('title')
    Welcome to Athena
@endsection

@section('judul1')
    Detail data cast
@endsection

@section('judul2')
    Berikut data yang akan ditampilkan
@endsection

@section('content')

    <h3>{{$cast->nama}}</h3>
    <h4>{{$cast->umur}}</h4>
    <p>{{$cast->bio}}</p>

    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection