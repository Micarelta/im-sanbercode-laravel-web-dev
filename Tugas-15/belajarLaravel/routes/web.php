<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\authController;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [homeController::class, 'utama']);

Route::get('/register', [authController::class, 'registerform']);

Route::post('/signup', [authController::class, 'signup']);

Route::get('/table', function(){
    return view('table');
});

Route::get('/data-tables', function(){
    return view('data-table');
});

//CRUD cast

//Create
//form tambah cast
Route::get('/cast/create', [castController::class, 'create']);
//untuk kirim atau tambah data ke database
Route::post('/cast', [castController::class, 'store']);

//Read
//menampilkan semua data
Route::get('/cast', [castController::class, 'index']);
//menampilkan detail berdasarkan id
Route::get('/cast/{cast_id}', [castController::class, 'show']);

//Update
//form update cast
Route::get('/cast/{cast_id}/edit', [castController::class, 'edit']);
//mengupdate data ke database berdasarkan id
Route::put('/cast/{cast_id}', [castController::class, 'update']);

//Delete
//menghapus data berdasarkan id
Route::delete('/cast/{cast_id}', [castController::class, 'destroy']);