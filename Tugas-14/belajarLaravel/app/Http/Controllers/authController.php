<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    public function registerform()
    {
        return view('register');
    }

    public function signup(Request $request)
    {
        $namaDepan = $request->input('firstname'); // lebih spesifik dalam hal penginputan
        $namaBelakang = $request->input('lastname'); // bisa berupa file atau text
        // bisa juga seperti dibawah
        // $namaDepan = $request['firstname'];
        // $namaBelakang = $request['lastname'];

        return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
