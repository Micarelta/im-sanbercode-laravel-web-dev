@extends('layout.master')

@section('title')
    Register Account
@endsection

@section('judul1')
    Buat Account Baru!
@endsection

@section('judul2')
    Sign Up Form
@endsection

@section('content')
    <form action="/signup" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="firstname"><br>
        <label>Last Name:</label><br>
        <input type="text" name="lastname"><br><br>

        <label>Gender:</label><br>
        <input type="radio">
        <label for="Male">Male</label><br>
        <input type="radio">
        <label for="Female">Female</label><br>
        <input type="radio">
        <label for="Other">Other</label><br><br>

        <label>Nationality:</label><br>
        <select>
            <option value="Indonesian">Indonesian</option>
            <option value="Australian">Australian</option>
            <option value="American">American</option>
            <option value="Mexican">Mexican</option>
        </select>
        <br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br><br>

        <label>Bio:</label><br>
        <textarea rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">
    </form>
    
@endsection