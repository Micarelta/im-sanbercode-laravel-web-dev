<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Sanberbook</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/signup" method="post">
        @csrf
        <p><label>First Name:</label></p>
        <input type="text" name="firstname">
        <p><label>Last Name:</label></p>
        <input type="text" name="lastname">

        <p><label>Gender:</label></p>
        <input type="radio">
        <label for="Male">Male</label><br>
        <input type="radio">
        <label for="Female">Female</label><br>
        <input type="radio">
        <label for="Other">Other</label><br>

        <p><label>Nationality:</label></p>
        <select>
            <option value="Indonesian">Indonesian</option>
            <option value="Australian">Australian</option>
            <option value="American">American</option>
            <option value="Mexican">Mexican</option>
        </select>

        <p><label>Language Spoken:</label></p>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br>

        <p><label>Bio:</label></p>
        <textarea rows="10" cols="30"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>