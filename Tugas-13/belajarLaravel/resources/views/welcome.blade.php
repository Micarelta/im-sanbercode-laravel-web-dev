@extends('layout.master')

@section('title')
    Registrasi Sukses
@endsection

@section('judul1')
    Akun anda berhasil dibuat !
@endsection

@section('judul2')
    #Reminder
@endsection

@section('content')
    <h1>Selamat Datang {{$namaDepan}} {{$namaBelakang}}</h1>
    <h3>Terima Kasih telah bergabung di Sanberbook. Social Media kita bersama!</h3>
@endsection